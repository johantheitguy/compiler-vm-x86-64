#!/bin/zsh

zsh build/_common.sh

vagrant ssh -c "bash remote-scripts/screen.sh"
vagrant scp :/lib64/libtinfo.so.5.9 output/lib/libtinfo.so.5
vagrant scp :screen-4.9.1/screen output/bin/
