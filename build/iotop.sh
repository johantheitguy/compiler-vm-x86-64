#!/bin/zsh

zsh build/_common.sh

vagrant ssh -c "bash remote-scripts/iotop.sh"
vagrant scp :iotop-1.25/iotop output/bin/
