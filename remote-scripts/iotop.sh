wget https://github.com/Tomas-M/iotop/releases/download/v1.25/iotop-1.25.tar.xz
tar xf iotop-1.25.tar.xz
cd iotop-1.25
sed -i 's/-Wdate-time/-Wno-strict-overflow/' Makefile
make -j
