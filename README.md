
# Cross-Compilation VM for x86-64 on a Silicon Mac (for my Synology DiskStation)

This project automates the creation of a Linux virtual machine on an ARM silicon macOS, which is then used to cross-compile binaries for a Synology DiskStation.

## Overview

This repository contains scripts and configuration files to set up a Linux virtual machine on a macOS host. The VM is specifically configured for cross-compiling software to be used on Synology DiskStation NAS devices.

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Before starting, ensure you have the following installed:
- macOS (10.15 or later)

- HomeBrew https://brew.sh/
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

- Vagrant https://www.vagrantup.com/
```bash
brew install vagrant
```

#### 
- The QEMU virtual machine manager https://www.qemu.org/download/#macos

```bash
brew install qemu
```

- The QEMU provider for vagrant https://github.com/ppggff/vagrant-qemu

```bash
vagrant plugin install vagrant-qemu
```

- The Vagrant SCP plugin https://github.com/invernizzi/vagrant-scp

```bash
vagrant plugin install vagrant-scp
```

### Installation

1. Clone the repository:
   ```
   git clone https://gitlab.com/johantheitguy/compiler-vm-x86-64.git
   ```
2. Navigate to the project directory:
   ```
   cd compiler-vm-x86-64
   ```
3. Start and provision the VM:
   ```
   vagrant up
   ```
4. Run any of the build scripts for the tools you are looking for:
   ```
   build/iotop.sh
   build/screen.sh
   ```
5. Inspect the build system state (optional)
   ```
   vagrant ssh
   ```
6. Copy the files to the target system
7. Set the LD_LIBRARY_PATH to the lib folder
   ```
   export LD_LIBRARY_PATH=...
   ```
8. Set the PATH to the bin folder
   ```
   export PATH=$PATH:...
   ```
9. Kill the vm
   ```
   vagrant destroy
   ```

## Configuration

The `Vagrantfile` in the root of this project configures the virtual machine. Modify it as needed to change the VM settings like CPU, memory, and network configurations.

## Contributing

Just create a MR :)

## License

This project is licensed under the MIT License - see the `LICENSE.md` file for details.

## Acknowledgments

Our many thanks to the developers of the following projects that have made this possible:

- HomeBrew https://brew.sh/
- Vagrant https://www.vagrantup.com/
- QEMU https://www.qemu.org/download/#macos
- QEMU Vagrant Provider https://github.com/ppggff/vagrant-qemu
- Vagrant SCP Plugin https://github.com/invernizzi/vagrant-scp

That and all the world of wonder that is Linux and GNU. 
